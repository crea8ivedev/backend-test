<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetails extends Model
{
    protected $table = 'orderdetails';
    protected $appends = ['line_total'];

    public function getLineTotalAttribute()
    {
        return number_format($this->quantityOrdered * $this->priceEach, 2, '.', '');
    }
   
    public function products()
    {
        return $this->belongsTo('App\Product','productCode','productCode');
    }
    
}
