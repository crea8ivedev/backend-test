<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    public function order_details()
    {
        return $this->hasManyThrough(
            'App\Product',
            'App\OrderDetails',
            'orderNumber',
            'productCode',
            'orderNumber',
            'productCode')
            ->select('productName as product','productLine as product_line','priceEach as unit_price','quantityOrdered as qty');
    }

    public function customer()
    {
        return $this->belongsTo('App\Customer','customerNumber','customerNumber')
        ->select('contactFirstName as first_name','contactLastName as last_name','phone','country as country_code','customerNumber');
    }
    
}
