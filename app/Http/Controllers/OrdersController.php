<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Order;

class OrdersController extends Controller
{

    /**
     * fetch order details
     * @param $id
     * @return array
     */
    public function fetchOrderData($id)
    {        
        // your logic goes here. 
        $result = Order::where('orderNumber',$id)
        ->with('order_details','customer')
        ->first();
        if($result){
            $result = $result->makeHidden('orderDate');
            $result = $result->makeHidden('orderNumber');
            $result = $result->makeHidden('requiredDate');
            $result = $result->makeHidden('shippedDate');
            $result = $result->makeHidden('comments');
            $result = $result->makeHidden('customerNumber');
            $result = $result->makeHidden('customerNumber');
            $result->order_id = $result->orderNumber;
            $result->order_date = $result->orderDate;
            $result->order_details = $result->order_details->makeHidden('laravel_through_key');
            foreach ($result->order_details as $key => $value) {
                $amount = $value->qty * $value->unit_price;
                $value->line_total=  number_format($amount, 2, '.', '');
                $result->bill_amount+=  $amount;
            } 
            $result->bill_amount=  number_format($result->bill_amount, 2, '.', '');
            return response()->json($result,200);
         }
    else{
        return response()->json([],400);
    }
}
}